package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	public void init() throws ServletException{
		System.out.println("******************************************");
		System.out.println("UserServlet has been initialized");
		System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		// System properties for first name
		System.getProperties().put("fname", req.getParameter("fname").toString());
		
		// HttpSession for last name
		HttpSession lname = req.getSession();
		lname.setAttribute("lname", req.getParameter("lname").toString());
		
		// Servlet Context Attribute for the email
		ServletContext email = getServletContext();
		email.setAttribute("email", req.getParameter("email").toString());
		
		// URL rewrtiting via sendRedirect
		res.sendRedirect("details?contact="+req.getParameter("contact").toString());
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println("UserServlet has been destroyed");
		System.out.println("******************************************");
	}

}
